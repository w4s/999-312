$(function () {
	// Owl.Carousel home-page
	$('.owl-home').owlCarousel({
		loop: true,
		nav: true,
		items: 1,
		autoplay: true,
		autoplayTimeout: 8000,
		autoplayHoverPause: true
	});
	// Owl.Carousel Testimonials
	$('.owl-testimonials').owlCarousel({
		center: true,
		loop: true,
		nav: true,
		items: 1,
		autoplay: true,
		autoplayTimeout: 8000,
		autoplayHoverPause: true
	});
	// Owl.Carousel shops
	$('.slider-shop__wrap').owlCarousel({
		items: 2,
		nav: true,
		dots: false,
		loop: false
	});
	//Кнопка "Наверх"
	//Документация:
	//http://api.jquery.com/scrolltop/
	//http://api.jquery.com/animate/
	$(window).scroll(function () {
		if ($(this).scrollTop() > 0) {
			$('#top').fadeIn();
		} else {
			$('#top').fadeOut();
		}
	});
	$("#top").click(function () {
		$("body, html").animate({
			scrollTop: 0
		}, 800);
		return false;
	});

	//Toggle mobile menu
	$('.toggle-mobile-menu').click(function () {
		$('.mobile-menu').toggle('slow');
		return false;
	});

	//Initialization Maskedinput
	$('input[type="tel"]').mask('+7 (999) 999-9999');
	$('input[name="telephone"]').mask('+7 (999) 999-9999');

	//Mail
	$(".main-form").submit(function (e) {
		var formData = new FormData($(this)[0]);

		$.ajax({
			url: 'scripts/mail-order.php',
			type: "POST",
			data: formData,
			async: false,
			success: function (msg) {
				$.fancybox.close();
				$(".animation-popup-order").css('visibility', 'visible');
				$('.animation-popup-order').addClass('animation-left');
				setTimeout(function () {
					alert(msg);
					$('.animation-popup-order').css('visibility', 'hidden');
					$('.animation-popup-order').removeClass('animation-left');
				}, 2500);
			},
			error: function (msg) {
				alert('Ошибка!');
			},
			cache: false,
			contentType: false,
			processData: false
		});
		e.preventDefault();
	});
	//Запчасти для модели
	$('.model-list__item').click(function () {
		$('.popup-auto__model span').text($(this).find('span').attr('data-company') + ' ' + $(this).find('span').text());
		$('.popup-auto__btn').attr('data-company', $(this).find('span').attr('data-company'));
		$('.popup-auto__btn').attr('data-model', $(this).find('span').text());
		$.fancybox.open($('#popup-auto'), {
			touch: false
		});
		return false;
	});
	$('.popup-auto__btn').click(function () {
		$.fancybox.close($('#popup-auto'));
		$('input[name="Марка"]').val($(this).attr('data-company'));
		$('input[name="Модель"]').val($(this).attr('data-model'));
	});
	//Быстрый заказ
	$('button[data-src="#popup-fast-buy"]').click(function () {
		$('#popup-fast-buy input[name="Товар"]').val($(this).attr('data-title'));
		$('#popup-fast-buy input[name="Цена"]').val($(this).attr('data-price'));
	});
	//Custom filter
	$('.custom-filter').each(function () {
		var strGET = window.location.search.replace('?', '');
		if (decodeURIComponent(strGET).includes($(this).val())) {
			$(this).attr('checked', 'checked');
		}
	});
	$('.custom-filter').click(function () {
		if ($(this).attr('checked') == 'checked') {
			window.location.href = window.location.pathname;
			$(this).attr('checked') == '';
		} else {
			window.location.href = window.location.pathname + '?ajaxfilter=search,' + $(this).val();
			$(this).attr('checked', 'checked');
		}
	});
	//Наличие в магазинах
	$('a[href="http://999-312.ru/index.php?route=checkout/checkout"]').click(function () {
		if ($('.header-product-in-stock').length > 1) {
			if ($('.header-product-in-stock__one').length === 1 && $('.header-product-in-stock__two').length === 1
				|| $('.header-product-in-stock__one').length === 1 && $('.header-product-in-stock__three').length === 1
				|| $('.header-product-in-stock__two').length === 1 && $('.header-product-in-stock__three').length === 1) {
				alert('ВНИМАНИЕ! \nВыбранные товары находятся в разных магазинах, \nоформите доставку или свяжитесь с нами +7-(8412)-999-312');
			}
		}
	});
});

//Скрипты для shipping_method.twig
// $(document).ready(function () {
// 	$('input[value="pickup.pickup"]').change(function () {
// 		if ($('.header-product-in-stock').length > 1) {
// 			if ($('.header-product-in-stock__one').length === 1 && $('.header-product-in-stock__two').length === 1
// 				|| $('.header-product-in-stock__one').length === 1 && $('.header-product-in-stock__three').length === 1
// 				|| $('.header-product-in-stock__two').length === 1 && $('.header-product-in-stock__three').length === 1) {
// 				alert('ВНИМАНИЕ! \nВыбранные товары находятся в разных магазинах, \nоформите доставку или свяжитесь с нами +7-(8412)-999-312');
// 			}
// 		}
// 	});
// 	//Add field for shipping method
// 	$('.shipping_method').change(function () {
// 		if (this.value == 'free.free' || this.checked == 'checked') {
// 			$('.custom_address_1').show();
// 		} else {
// 			$('.custom_address_1').hide();
// 		}
// 	});
// 	$('.custom_address_1').on('input', function (e) {
// 		$('#input-payment-address-1').attr('value', $(e.target).val());
// 	});
// });